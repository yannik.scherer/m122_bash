import requests
import csv
from datetime import datetime

# Definiere das Depot
depot = {
    'Novartis': {'quantity': 10, 'buy_price': 80},  # fiktiver Kaufpreis in CHF
    'Nestle': {'quantity': 15, 'buy_price': 100},
    'ABB': {'quantity': 20, 'buy_price': 25},
    'Swisscom': {'quantity': 5, 'buy_price': 500},
    'USD': {'quantity': 3000, 'buy_price': 0.92},  # CHF pro USD
    'Bitcoin': {'quantity': 0.1, 'buy_price': 30000}  # CHF pro Bitcoin
}

# Funktion zum Abrufen der aktuellen Kurse
def get_current_prices():
    prices = {}
    # Fiktive API-Endpoints für die Aktienkurse
    stocks = ['Novartis', 'Nestle', 'ABB', 'Swisscom']
    for stock in stocks:
        response = requests.get(f'https://api.example.com/stock/{stock}')
        data = response.json()
        prices[stock] = data['current_price']

    # Fiktive API-Endpoint für den USD-Kurs
    response = requests.get('https://api.example.com/currency/USDCHF')
    data = response.json()
    prices['USD'] = data['current_price']

    # Fiktive API-Endpoint für den Bitcoin-Kurs
    response = requests.get('https://api.example.com/crypto/bitcoin')
    data = response.json()
    prices['Bitcoin'] = data['current_price']

    return prices

# Funktion zur Berechnung des Depotwerts in CHF
def calculate_depot_value(prices, depot):
    total_value = 0
    for asset, info in depot.items():
        current_price = prices.get(asset)
        if current_price:
            total_value += info['quantity'] * current_price
    return total_value

# Historische Werte speichern
def save_historical_value(value, filename='depot_history.csv'):
    now = datetime.now()
    with open(filename, 'a', newline='') as csvfile:
        fieldnames = ['timestamp', 'value']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow({'timestamp': now, 'value': value})

# Hauptskript
if __name__ == "__main__":
    prices = get_current_prices()
    depot_value = calculate_depot_value(prices, depot)
    save_historical_value(depot_value)
    print(f"Der aktuelle Wert des Depots in CHF beträgt: {depot_value}")

