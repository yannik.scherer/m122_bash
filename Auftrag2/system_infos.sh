#!/bin/bash

# Funktion zum Abrufen der Systeminformationen und Generierung einer Textausgabe
get_system_info() {
    echo "------------------------------------------------------"
    echo "System Information Report"
    echo "------------------------------------------------------"
    echo "Current System Time: $(date)"
    echo "Uptime: $(uptime -p)"
    echo "Hostname: $(hostname)"
    echo "IP Address: $(hostname -I | awk '{print $1}')"
    echo "Operating System: $(uname -s)"
    echo "OS Version: $(uname -r)"
    echo "CPU Model: $(lscpu | grep 'Model name' | awk -F ':' '{print $2}' | xargs)"
    echo "CPU Cores: $(nproc)"
    echo "Total Memory: $(free -h | grep 'Mem:' | awk '{print $2}')"
    echo "Used Memory: $(free -h | grep 'Mem:' | awk '{print $3}')"
    echo "Free Memory: $(free -h | grep 'Mem:' | awk '{print $4}')"
    echo "Disk Usage:"
    df -h --total | grep 'total'
    echo "------------------------------------------------------"
}

# Funktion zum Abrufen der Systeminformationen und Generierung einer HTML-Datei
get_system_info_html() {
    echo "<html>"
    echo "<head><title>System Information Report</title></head>"
    echo "<body>"
    echo "<h1>System Information Report</h1>"
    echo "<p>Current System Time: $(date)</p>"
    echo "<p>Uptime: $(uptime -p)</p>"
    echo "<p>Hostname: $(hostname)</p>"
    echo "<p>IP Address: $(hostname -I | awk '{print $1}')</p>"
    echo "<p>Operating System: $(uname -s)</p>"
    echo "<p>OS Version: $(uname -r)</p>"
    echo "<p>CPU Model: $(lscpu | grep 'Model name' | awk -F ':' '{print $2}' | xargs)</p>"
    echo "<p>CPU Cores: $(nproc)</p>"
    echo "<p>Total Memory: $(free -h | grep 'Mem:' | awk '{print $2}')</p>"
    echo "<p>Used Memory: $(free -h | grep 'Mem:' | awk '{print $3}')</p>"
    echo "<p>Free Memory: $(free -h | grep 'Mem:' | awk '{print $4}')</p>"
    echo "<h2>Disk Usage:</h2>"
    echo "<pre>$(df -h --total | grep 'total')</pre>"
    echo "</body>"
    echo "</html>"
}

# Variablen für die Optionen
output_file=false
html_output=false

# Optionen analysieren
while getopts ":fh" opt; do
  case $opt in
    f)
      output_file=true
      ;;
    h)
      html_output=true
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
  esac
done

# Verzeichnis und Dateiname festlegen
script_dir=$(dirname "$(realpath "$0")")
logfile="$script_dir/system_info.log"
htmlfile="$script_dir/system_info.html"

# Systeminformationen in eine Textdatei schreiben, falls die Option -f gesetzt ist
if $output_file; then
    get_system_info | tee -a "$logfile"
fi

# Systeminformationen in eine HTML-Datei schreiben, falls die Option -h gesetzt ist
if $html_output; then
    get_system_info_html > "$htmlfile"
fi

# Systeminformationen auf der Konsole anzeigen, falls keine Option gesetzt ist
if [ $output_file = false ] && [ $html_output = false ]; then
    get_system_info
fi

# HTML-Datei automatisch im Standard-Webbrowser öffnen, falls die Option -h gesetzt ist
if $html_output; then
    xdg-open "$htmlfile" 2>/dev/null || open "$htmlfile"
fi

# Beispiel-Eintrag im Crontab für die Ausführung alle 5 Minuten
# */5 * * * * /home/user/scripts/system_info.sh -f -h
