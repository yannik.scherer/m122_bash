#!/bin/bash

mkdir -p ./_templates

touch ./_templates/datei-1.txt
touch ./_templates/datei-2.pdf
touch ./_templates/datei-3.doc

mkdir -p ./_schulklassen

schueler_namenTBZ=("Anna Müller" "Max Schmidt" "Sophie Fischer" "David Weber" "Emma Wagner" "Luca Becker" "Lena Hoffmann" "Jonas Schäfer" "Laura Koch" "Tim Richter" "Julia Bauer" "Felix Klein" "Sarah Wolf" "Paul Schröder" "Hannah Neumann")
schueler_namenSekundarstufe=("Simon Schwarz" "Marie Zimmermann" "Nico Braun" "Lisa Krüger" "Daniel Hofmann" "Mia Meier" "Leon Lehmann" "Sandra Vogel" "Tobias Sommer" "Clara Schmitt" "Jan Keller" "Emily Herrmann" "Lucas Jung" "Johanna Lange" "Tom Walter")
schueler_namenAusbildungszentrum=("Vanessa Peters" "Alexander Möller" "Victoria Schulz" "Christian Wagner" "Lea Huber" "Kevin Kaiser" "Selina Schmid" "Patrick Marx" "Elena Berger" "Moritz Hartmann" "Katharina Köhler" "Sebastian Vogt" "Franziska Friedrich" "Philipp Vogel" "Michelle Schuster")

echo "${schueler_namenTBZ[@]}" | tr ' ' '\n' > ./_schulklassen/M122-AP23d.txt
echo "${schueler_namenSekundarstufe[@]}" | tr ' ' '\n' > ./_schulklassen/AB3b.txt
echo "${schueler_namenAusbildungszentrum[@]}" | tr ' ' '\n' > ./_schulklassen/RAU.txt

echo "Verzeichnisse und Dateien wurden erfolgreich erstellt."
